
public class Merch {
	String name, unit; //nazwa towaru, jednostka
	int unit_qnt; // ilosc jednostek
	double price_single_net, percent; //cena jednostkowa netto, procent
	
	
	public double price_single_gross(double single, double percent) {  //cena jednostkowa brutto
		
		return single+(single*(percent/100));
		
	}
	
	public double price_whole_gross(double single, double percent,int  un_qt) { // cena całkowita brutto
		
		return (single+(single*(percent/100)))*un_qt;
		
	}
	
	public double price_whole_net(double single,int un_qt) { // cena całkowita netto
		return single*un_qt;
	}
}
