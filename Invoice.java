
public class Invoice extends Merch{
	public static int invoice_number;  // numer faktury
	String date_sale, date_payment; // data sprzedazy , termin zapłaty
	int wynik =0 ; // zmienna pomicnicza

	public void days_to_payment(String dsale, String dpayment) { // yyyy-MM-dd [10znaków]-format daty

		// lata na int
		String syear = dsale.substring(0, 4);
		int isyear = Integer.parseInt(syear);
		String pyear = dpayment.substring(0, 4);
		int ipyear = Integer.parseInt(pyear);
		// miesiące na int
		String smonth = dsale.substring(5, 7);
		int ismonth = Integer.parseInt(smonth);
		String pmonth = dpayment.substring(5, 7);
		int ipmonth = Integer.parseInt(pmonth);
		// dni na int
		String sday = dsale.substring(8);
		int isday = Integer.parseInt(sday);
		String pday = dpayment.substring(8);
		int ipday = Integer.parseInt(pday);

		// zmienne pomocnicza
		boolean czy=true;

		// zakładam że każy miesiąc ma 30 dni
		if (ipyear < isyear) {
			System.out.println("Termin zapłaty jest wcześniejsza od daty sprzedaży!");
			czy=false;
		} else {
			if ((ipyear - isyear) > 0) {
				if (ipmonth == 1) {
					wynik = (30 - isday) + ((12 - ismonth) * 30) + ipday + ((ipmonth - 1) * 30)+(((ipyear - isyear) - 1) * 360);
				} else
					wynik = (30 - isday) + ((12 - ismonth) * 30) + ipday + ((ipmonth) * 30)+(((ipyear - isyear) - 1) * 360);
			} else {
				if (ipmonth < ismonth) {
					System.out.println("Data zapłaty jest wcześniejsza od daty sprzedaży!");
					czy=false;
				} else {
					if(ipday < isday) {
						System.out.println("Data zapłaty jest wcześniejsza od daty sprzedaży!");
						czy=false;
					} else wynik = (30 - isday)+((ipmonth-ismonth-1)*30)+ipday;;
				}
			}

		}
		if(czy)System.out.println("Do daty zapłaty pozostało: " + wynik + " dni!");
	}
}
